def print_first_last_mid(st1):
    print(st1[0])
    print(st1[-1])
    if len(st1)%2==1:
        print(st1[len(st1)//2+1])

def sub_numbers_in_str(st2):
    sum=0
    for i in st2:
        if i.isdigit():
            sum=sum+int(i)
    return sum

# ex1
#st1=input('Введите строку: ')
#print_first_last_mid(st1)

# ex12
#st2=input('Введите строку: ')
#print(sub_numbers_in_str(st2))
