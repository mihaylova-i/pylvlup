def arithmetic(a, b, op):
    if op=='+':
        c=a+b
    elif op=='-':
        c=a-b
    elif op=='*':
        c=a*b
    elif op=='/':
        if b==0:
            c='бесконечность'
        else:
            c=a/b
    else:
        c='неизвестная операция'
    return c

def is_year_leap(year):
    if year%4==0:
        if year%100==0:
            if year%400==0:
                return 'високосный'
            else:
                return 'НЕвисокосный'
        else:
            return 'високосный'
    else:
        return 'НЕвисокосный'

def month(m):
    if m==1:
        return 'январь'
    elif m==2:
        return 'февраль'
    elif m==3:
        return 'март'
    elif m==4:
        return 'апрель'
    elif m==5:
        return 'май'
    elif m==6:
        return 'июнь'
    elif m==7:
        return 'июль'
    elif m==8:
        return 'август'
    elif m==9:
        return 'сентябрь'
    elif m==10:
        return 'октябрь'
    elif m==11:
        return 'ноябрь'
    elif m==12:
        return 'декабрь'
    else:
        return 'Error'

def print_cube_and_square(c):
    print('квадрат числа: ', c*c)
    print('куб числа: ', c*c*c)

# ex1
#a=int(input('Введите a: '))
#b=int(input('Введите b: '))
#op=input('Введите операцию: ')
#c=arithmetic(a, b, op)
#print(c)

# ex2
#year=int(input('Введите год для проверки: '))
#print(is_year_leap(year))

# ex3
#m=int(input('Введите порядковый номер месяца: '))
#print(month(m))

# ex4
#c=int(input('Введите целое число для вчисления: '))
#print_cube_and_square(c)


