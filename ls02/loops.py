def print_hi(n):
    for i in range(n):
        print('Hi, friend!')

def print_numbers(start, end, step):
    for i in range(start, end+1, step):
        print(i)

def print_count_down_100(i):
    while i>0:
        print (i)
        i=i-4

def count_odd_num(n):
    k=0
    while n>0:
        m=n%10
        if m%2==1:
            k=k+1
        n=n//10
    return k

# ex1
#n=int(input('Введите число: '))
#print_hi(n)

# ex2
#start=int(input('Введите начльное число: '))
#end=int(input('Введите конечное число: '))
#step=int(input('Введите шаг: '))
#print_numbers(start, end, step)

# ex3
#print_count_down_100(100)

# ex9
n=int(input('Введите натуральное число: '))
print(count_odd_num(n))
