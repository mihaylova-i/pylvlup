def list_count_num(my_list, num):
    return my_list.count(num)

def list_count_word(my_list, word):
    return my_list.count(word)

# ex1
#str = input('Введите числа через ", ": ')
#my_list = str.split(', ')
#num = input('Введите число для проверки: ')
#print(list_count_num(my_list, num))

# ex2
str = input('Введите слова через ", ": ')
my_list = str.split(', ')
word = input('Введите слово для проверки: ')
print(list_count_word(my_list, word))
